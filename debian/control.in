Source: php-pecl-http-2
Section: web
Priority: optional
Maintainer: Debian PHP PECL Maintainers <team+php-pecl@tracker.debian.org>
Uploaders: Ulises Vitulli <dererk@debian.org>,
           Facundo Guerrero <guerremdq@gmail.com>,
           Ondřej Surý <ondrej@debian.org>
Build-Depends: chrpath,
               debhelper (>= 10~),
               dh-php (>= 4~),
               libcurl4-openssl-dev,
               libevent-dev,
               libicu-dev,
               php5.6-dev,
               php5.6-propro,
               php5.6-raphf,
               zlib1g-dev
Standards-Version: 4.5.1
Homepage: http://pecl.php.net/package/pecl_http
Vcs-Git: https://salsa.debian.org/php-team/pecl/php-pecl-http.git
Vcs-Browser: https://salsa.debian.org/php-team/pecl/php-pecl-http
X-PHP-Dummy-Package: no
X-PHP-Default-Version: 5.6
X-PHP-Versions: 5.6
X-PHP-PECL-Name: http

Package: php-http
Architecture: any
Pre-Depends: php-common (>= 2:69~)
Depends: ${misc:Depends},
         ${pecl:Depends},
         ${php:Depends},
         ${shlibs:Depends}
Provides: php-pecl-http,
          ${pecl:Provides},
          ${php:Provides}
Breaks: ${pecl:Breaks}
Replaces: ${pecl:Replaces}
Suggests: ${pecl:Suggests}
Description: PECL HTTP module for PHP Extended HTTP Support
 This HTTP extension aims to provide a convenient and powerful set of
 functionality for one of PHPs major applications.
 .
 It eases handling of HTTP urls, headers and messages, provides means
 for negotiation of a client&apos;s preferred content type, language
 and charset, as well as a convenient way to send any arbitrary data
 with caching and resuming capabilities.
 .
 It provides powerful request functionality with support for parallel
 requests.
